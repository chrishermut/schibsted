import './data-source.css';

import React, { FormEvent } from 'react';

import { DataSourceProps } from './models';

export default class DataSource extends React.Component<DataSourceProps> {
  constructor(props: DataSourceProps) {
    super(props);
  }

  updateDataSource(event: FormEvent<HTMLFormElement>): void {
    event.nativeEvent.stopPropagation();

    this.props.setDataSource({
      fashion: (event.currentTarget.elements.namedItem('fashion') as HTMLInputElement).checked,
      sports: (event.currentTarget.elements.namedItem('sports') as HTMLInputElement).checked,
    })
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {
    return <div className="data-source-container">
      <strong>Data sources</strong>
      <form onChange={this.updateDataSource.bind(this)}>
        <div className="data-source-input">
          <input type="checkbox" name="fashion" defaultChecked={this.props.dataSource.fashion} />
          <label htmlFor="fashion">Fashion</label>
        </div>
        <div className="data-source-input">
          <input type="checkbox" name="sports" defaultChecked={this.props.dataSource.sports} />
          <label htmlFor="sports">Sports</label>
        </div>
      </form>
    </div>
  }
}