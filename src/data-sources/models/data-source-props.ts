export interface DataSource {
  fashion: boolean;
  sports: boolean;
}

export interface DataSourceProps {
  readonly dataSource: DataSource;
  readonly setDataSource: SetSourceFn;
}

interface SetSourceFn {
  (dataSource: DataSource): void
}