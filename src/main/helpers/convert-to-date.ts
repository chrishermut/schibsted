export function convertToDate(norskDateString: string) {
  const dateString = norskDateString.split('.')
    .map(e => {
      e.trim();
      return [...e.split(' ').filter(e => e)]
    }).flat().reduceRight((acc, value, index) => {
      if (index === 1) {
        // @ts-ignore
        acc.push(norskMonthsDictionary[value])
      } else {
        // @ts-ignore
        acc.push(value)
      }

      return acc;
    }, []).join('.');

  console.log(dateString, Date.parse(dateString))

  return Date.parse(dateString);
}

const norskMonthsDictionary = {
  januar: '01',
  februar: '02',
  mars: '03',
  april: '04',
  mai: '05',
  juni: '06',
  juli: '07',
  august: '08',
  september: '09',
  oktober: '10',
  november: '11',
  desember: '12'
};