import { forkJoin, from, Observable, of, throwError } from 'rxjs';
import { delay, mapTo, switchMap, tap } from 'rxjs/operators';

import { DataSource } from '../../data-sources/models';

export function getData$(dataSource: DataSource, dataEndpoint: string): Observable<any> {
  return forkJoin(
    Object.entries(dataSource)
      .filter(entry => entry[1])
      .map((entry => fetch(`${dataEndpoint}/articles/${entry[0]}`))
      ))
    .pipe(
      switchMap((responses: Response[]): any => from(Promise.all(responses.map(response => response.json())))),
      switchMap((data: any) => {
        if (data.find((e: any) => e.message && e.message === 'Ooooops! this shouldn\'t be shown to clients probably...')) {
          return throwError('Bad response');
        } else {
          return of(data);
        }
      }),
      delay(1000)
    )
}