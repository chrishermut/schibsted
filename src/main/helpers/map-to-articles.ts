import { convertToDate } from '.';
import { Article, ArticleFromResponse } from '../models';

export function mapToArticles(articles: ArticleFromResponse[]): Article[] {
  return articles.map(article => ({
    id: article.id,
    date: new Date(convertToDate(article.date)),
    imageUrl: article.image,
    category: article.category,
    title: article.title,
    preamble: article.preamble
  }))
}