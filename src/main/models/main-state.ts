import { DataSource } from '../../data-sources/models';
import { Article } from '../models/article';

export class MainState {
  data: Data = {
    loading: false,
    source: { fashion: true, sports: false }
  }
  articles: Article[] = [];
  sortOrder: 'DESCENDING' | 'ASCENDING' = 'DESCENDING';
}

export interface Data {
  loading: boolean;
  source?: DataSource;
  status?: 'ERROR' | 'OK',
}