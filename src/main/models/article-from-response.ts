export interface ArticleFromResponse {
  id: number;
  date: string;
  image: string;
  category: string;
  title: string;
  preamble: string;
}