export interface Article {
  id: number;
  date: Date;
  imageUrl: string;
  category: string;
  title: string;
  preamble: string;
}