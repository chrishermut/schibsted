import './main.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Subscription } from 'rxjs';

import ArticleList from '../article-list/article-list';
import DataSource from '../data-sources/data-source';
import { DataSource as DataSourceModel } from '../data-sources/models';
import NotificationPortal from '../notification/notification';
import Sort from '../sort/sort';
import { mapToArticles } from './helpers';
import { getData$ } from './helpers';
import { MainProps, MainState } from './models';

const dataEndpoint = 'http://localhost:6010';

export default class Main extends React.Component<MainProps, MainState> {
  constructor(props: MainProps) {
    super(props);

    this.state = new MainState();
  }

  toggleSortOrder() {
    this.setState({
      sortOrder: this.state.sortOrder === 'DESCENDING' ? 'ASCENDING' : 'DESCENDING',
      articles: this.state.articles.sort(this.state.sortOrder === 'DESCENDING' ? ((a, b) => a.date < b.date ? -1 : 1) : ((a, b) => a.date > b.date ? -1 : 1))
    });
  }

  setDataSource = (dataSource: DataSourceModel) => {
    this.setState({
      data: {
        loading: true,
        source: dataSource
      },
      articles: []
    });

    if (!Object.values(dataSource).find(v => v)) {
      this.setState({
        data: {
          loading: false
        }
      })

      return;
    }

    const subscription: Subscription = getData$(dataSource, dataEndpoint).subscribe(data => {
      this.setState({
        data: {
          loading: false
        },
        articles: mapToArticles(data.map((articleSet: any) => articleSet.articles ? articleSet.articles : []).flat())
      })

      this.toggleSortOrder();
    },
      error => this.setState({
        data: {
          ...this.state.data,
          loading: false,
          status: 'ERROR'
        }
      }),
      () => subscription.unsubscribe()
    );
  }

  componentDidMount() {
    this.setDataSource(this.state.data.source!);
  }

  render() {
    const main = <React.Fragment>
      <ArticleList articles={this.state.articles} />
    </React.Fragment>;

    const loadingData = <div style={{
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    }}>
      <h1>Loading data...</h1>
    </div>;

    return <React.Fragment>
      {this.state.data.status === 'ERROR'
        ? <NotificationPortal>
          <div style={{ display: 'flex', justifyContent: 'center', backgroundColor: 'red', padding: '0.5rem' }}>
            <strong>
              Error loading data
            </strong>
          </div>
        </NotificationPortal>
        : <Sort toggleSortOrder={this.toggleSortOrder.bind(this)} />
      }

      <div className="main-container">
        <DataSource setDataSource={this.setDataSource} dataSource={this.state.data.source!} />
        {this.state.data.loading ? loadingData : main}
      </div>
    </React.Fragment>
  }
}