import ReactDOM from 'react-dom';

export default function NotificationPortal(props: any) {
  return ReactDOM.createPortal(props.children, document.getElementById('notifications-container') as HTMLDivElement);
}
