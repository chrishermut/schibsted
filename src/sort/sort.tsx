import './sort.css';

import React from 'react';

import { SortProps } from './models';

export default function Sort(props: SortProps) {
  return <div className="sort-container">
    <strong onClick={() => props.toggleSortOrder()}>Sort by date</strong>
  </div>
}