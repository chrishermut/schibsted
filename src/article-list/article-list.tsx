import './article-list.css';

import React from 'react';

import ArticlePreview from '../article-preview/article-preview';
import { ArticleListProps } from './models';


export default function ArticleList(props: ArticleListProps) {
  return <div className="article-list-container">
    {props.articles.map(article => <ArticlePreview key={article.id} article={article} />)}
  </div>
}