import { Article } from '../../main/models';

export interface ArticleListProps {
  articles: Article[];
}