import './article-preview.css';

import React from 'react';

import ArticlePreviewProps from './models/article-preview-props';

export default function ArticlePreview(props: ArticlePreviewProps) {
  return <div className="article-preview-container">
    <div className="image-container" style={{ backgroundImage: `url("${props.article.imageUrl}")` }}>

    </div>
    <div className="content-container">
      <div className="header-container">
        <strong>{props.article.title}</strong>
        <span>{props.article.date.toDateString()}</span>
      </div>
      <div className="text-container">
        <p>{props.article.preamble}</p>
      </div>
    </div>
  </div>
}