import { Article } from '../../main/models';

export default interface ArticlePreviewProps {
  article: Article;
}